To spin up a sandbox

1. `mkdir mysandbox`
1. `cd mysandbox`
1. `npm install git+ssh://git@bitbucket.org:jilia/jilia-sandbox.git`
1. copy the following into app.js:

        var Sandbox = require('jilia-sandbox');
        var sandbox = new Sandbox('<clientKey:clientSecret>');
        sandbox.listen(0);

1. `node app.js`