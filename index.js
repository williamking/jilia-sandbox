var zetta = require('zetta'),
    uuid = require('node-uuid'),
    auth = require('zetta-peer-auth'),
    path = require('path'),
    fs = require('fs'),
    MemoryDeviceRegistry = require('./lib/memory_device_registry'),
    MemoryPeerRegistry = require('./lib/memory_peer_registry'),
    DeviceRegistry = require('./lib/device-registry'),
    PeerRegistry = require('./lib/peer-registry');

// recursively create a directory
function mkdir(p) {
  p = path.resolve(p);

  try {
    fs.mkdirSync(p);
  } catch(err) {
    if(err.code === 'ENOENT') {
      mkdir(path.dirname(p));
      mkdir(p);
    } else {
      var stat;
      try {
        stat = fs.statSync(p);
      } catch(err1) {
        throw err;
      }
      if(!stat.isDirectory()) {
        throw err;
      }
    }
  }
}

// devices is expected to be an array of arrays in the format
// [
//   [Constructor, param1, para2, ..],
//   [Constructor2, param1],
// ]
// This allows you to pass in a list of "fake" devices to be included in the sandbox
// To have persistent server and device ids, you can pass provide "persistenceId" in
// the options.  The persistence data will be stored in "cwd()/.data/<persistenceId>"
function JiliaSandbox(devices, opts) {
  opts = opts || {}

  // either use the provided name or create one
  var name = (opts.name || ('jilia-sandbox-' + uuid.v4()));

  var zettaOpts = {}
  if(opts.persistenceId) {
    var root = opts.dataRoot || process.cwd();
    var dir = path.join(root, '.data', opts.persistenceId);
    try {
      mkdir(dir);
    } catch(err) {
      console.log(err);
    }

    // persistence enabled, load name from directory
    try {
      name = fs.readFileSync(path.join(dir, 'id'), 'ascii');
      console.log("Loaded name: ", name);
    } catch(err) {
      fs.writeFileSync(path.join(dir, 'id'), name, 'ascii');
    }

    zettaOpts.registry = new DeviceRegistry(dir);
    zettaOpts.peerRegistry = new PeerRegistry(dir);
  } else {
    // persistence not enabled, use memory only
    zettaOpts.registry = new MemoryDeviceRegistry();
    zettaOpts.peerRegistry = new MemoryPeerRegistry();
  }

  var server = zetta(zettaOpts);
  server.name(name);

  if(opts.link) {
    var url = opts.link.url || 'https://api.jilia.io/v1';

    var useAuth = Object.keys(opts.link).indexOf('oauth') !== -1;
    if(useAuth) {

      var oauth = opts.link.oauth || url + '/oauth/accesstoken';
      server.use(auth({
        headers: {
          'Authorization': new Buffer(opts.link.credentials).toString('base64'),
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        url: oauth,
        method: 'POST',
        body: 'grant_type=client_credentials'
      }));

    }

    server.link(url);
  }

  if(devices) {
    devices.forEach(function(dev) {
      console.log('Creating ' + dev[1]);
      server.use.apply(server, dev);
    });
  }

  this.address;
  this.server = server;
}

JiliaSandbox.prototype.getServerAddress = function() {
  return this.server.httpServer.server.address();
};

JiliaSandbox.prototype.listen = function(port, cb) {
  this.server.listen.apply(this.server, arguments);
};

module.exports = JiliaSandbox;